﻿
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <cassert>
#define n 3

class pupil {
private:
    int i = 0;
    std::string name[n];
    std::string school, level, quant, gender;
    int birthDate[n], age = 0;
    int phone = 0;
public:
    void getName(std::string pupilName[n], std::string gender) {
        setlocale(0, "");
        /*std::cout << "Введите ФИО ребёнка: ";*/
        for (i = 0; i < n; i++) {
            name[i] = pupilName[i];
        }
    }
    void getBirthDate(int BirthDate[3], int Age) {
        for (int i = 0; i < 3; i++) {
            birthDate[i] = BirthDate[i];
        }
        age = Age;
    }
    void getPhoneNumber(int Num) {
        phone = Num;
    }
    void getEducationPlace(std::string School, std::string Class) {
        school = School;
        level = Class;
    }
    void getQuant(std::string Quant) {
        quant = Quant;
    }
    void showInfo() {
        using namespace std;
        setlocale(0, "");

        cout << "ФИО: " << name[0] << " " << name[1] << " " << name[2] << '\n';
        cout << "Дата рождения: " << birthDate << "(" << age << ")" << '\n';
        cout << "Место учёбы: " << school << '\n';
        cout << "Класс: " << level << '\n';
        cout << "Направление в Кванториуме: " << quant << '\n';
        cout << "Контактная информация: " << phone << '\n';
    }
};

int main()
{
    setlocale(0, "");


    pupil Peter;
    std::string name[n];
    std::string school, level, quant, gender;
    int birthDate[n], age;
    int phone;


    char choice = '0';


    while (choice != 7) {
        std::cout << "Выберите пункт меню: \n";
        std::cout << "\tВвести ФИО ребёнка - 1\n";
        std::cout << "\tВвести дату рождения - 2\n";
        std::cout << "\tУказать номер телефона - 3\n";
        std::cout << "\tМесто учёбы - 4\n";
        std::cout << "\tНаправление в Кванториуме - 5\n";
        std::cout << "\tВывести информацию об ученике - 6\n";
        std::cout << "\tВыход - 7\n";

        std::cin >> choice;
        if (isdigit(choice)) {

            if (choice == '7') {
                std::cout << "Всего доброго!";
                break;
            }


            else if (choice == '1') {

                std::string Name[250];
                char Nums[11] = { '0','1','2','3','4','5','6','7','8','9' };
                std::cout << "Введите ФИО ребёнка: ";
                for (int i = 0; i < n; i++) {
                    std::cin >> name[i]; 
                    for (int j = 0; j < name[i].size(); j++) {
                        int result = name[i].find(Nums[j]);
                        if (result == -1) {
                            continue;
                        }
                        else {
                            std::cout << "Введены некорректные данные.\nПовторите ввод.\n";
                            --i;
                            break;
                        }
                    }
                    
                }


                std::cout << "Укажите пол(м или ж): ";
                std::cin >> gender;
                Peter.getName(name, gender);
            }


            else if (choice == '2') {

                std::cout << "Введите дату рождения ребёнка: ";
                for (int i = 0; i < 3; i++) {
                    std::cin >> birthDate[i];
                }
                std::cin >> age;
                Peter.getBirthDate(birthDate, age);
            }
            else if (choice == '3') {

                std::cout << "Введите номер телефона ребёнка: ";
                std::cin >> phone;
                Peter.getPhoneNumber(phone);
            }
            else if (choice == '4') {

                std::cout << "Введите место учёбы ребёнка и класс: ";
                std::cin >> school;
                std::cin >> level;
                Peter.getEducationPlace(school, level);
            }
            else if (choice == '5') {

                std::cout << "Укажите направление обучения в Кванториуме: ";
                std::cin >> quant;
                Peter.getQuant(quant);
            }
            else if (choice == '6') {
                Peter.showInfo();
            }
            else {
                std::cout << "\n\nТакого пункта нет\n\n";
            }

        }

        else {
        std::cout << "Введите число!\n";
        std::cin >> choice;
        }

    }
}

